import React from 'react'
import { View, TouchableOpacity, Text, StyleSheet } from 'react-native'
import Icon from 'react-native-vector-icons/Feather';
const Task = (props) => (
    <TouchableOpacity
         onPress={()=> props.handleNavigation.navigate("Description", {todoData : props.item})}
         onLongPress={props.drag}
         style={styles.box}>
        <View style={styles.taskWrapper}>
            <TouchableOpacity onPress={() => props.setChecked()}>
                <Icon
                    name={props.checked ? "check" : "square"}
                    size={22}
                    color="#900"
                    style={{}}
                />
            </TouchableOpacity>

            <View>
                {props.checked && <View style={styles.verticalLine}></View>}
                <Text style={styles.task}>{props.title}</Text>
            </View>
        </View>
        <Icon
            name="trash-2"
            size={22}
            color="#900"
            style={{ marginLeft: 'auto' }}
            onPress={props.delete}
        />
    </TouchableOpacity>



)

export default Task

const styles = StyleSheet.create({
    taskWrapper: {
        marginTop: '5%',
        flexDirection: 'row',
        borderColor: '#FFFFFF',
        borderBottomWidth: 1.5,
        width: '100%',
        alignItems: 'stretch',
        minHeight: 40,
    },
    task: {
        paddingBottom: 20,
        paddingLeft: 10,
        marginTop: 6,
        borderColor: '#F0F0F0',
        borderBottomWidth: 1,
        fontSize: 15,
        color: 'grey',
    },
    verticalLine: {
        borderBottomColor: 'white',
        borderBottomWidth: 4,
        marginLeft: 10,
        width: '120%',
        position: 'absolute',
        marginTop: 15
    },
    box:{ 
        flexDirection: 'row', 
        justifyContent: 'space-between', 
        alignItems: 'center', 
        marginHorizontal: 15 ,
        backgroundColor: '#f5f3df',
        marginBottom: 8,
        borderRadius: 8,
        paddingHorizontal: 12
    }
})