import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import DescriptionScreen from '../screens/DescriptionScreen';

const Stack = createStackNavigator();

export default function HomeStack() {
  return (
    <Stack.Navigator initialRouteName={'Home'} headerMode={'none'} >
      <Stack.Screen name='Home' component={HomeScreen} />
      <Stack.Screen name='Description' component={DescriptionScreen} />
    </Stack.Navigator>
  );
}