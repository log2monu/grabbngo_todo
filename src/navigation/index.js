import React from 'react';
import { StatusBar } from 'react-native';
import { AuthProvider } from './AuthProvider';
import Routes from './Routes';

export default function Providers() {
  return (
    <AuthProvider>
      <StatusBar backgroundColor={'#e0c702'} barStyle={'dark-content'} />
      <Routes />
    </AuthProvider>
  );
}