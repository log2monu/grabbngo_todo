import React from 'react'
import {StyleSheet, ImageBackground, View, Text} from "react-native";
import moment from 'moment';

const DescriptionScreen = (props) => {
  const {todoData} = props.route.params;
  
  var a = moment(new Date(),'M/D/YYYY');
  var b = moment(todoData.date == "Pick Date" ? new Date : todoData.date,'M/D/YYYY');
  var diffDays = b.diff(a, 'days');

  var a = moment(new Date().toLocaleTimeString());
  
  var b = moment(todoData.date == "Pick Date" ?  (todoData.time, 'hh:mm:ss'): "-");
  console.warn("gvdsh :", a)
  var diffTime = b.diff(a, '');
  
  return (
    <>
      <ImageBackground source={require("../images/bg.jpg")} style={styles.container}>
        <View style={styles.card}>
          <Text style={{fontSize: 25, alignSelf: 'center', color: '#6646ee', marginVertical: 35}}>{todoData.title}</Text>
          <View style={{flexDirection :'row', justifyContent: 'space-between'}}>
            <View style={{}}>
              <Text style={{fontWeight: 'bold', fontSize: 14}}>Date</Text>
              <Text style={{ marginBottom: 20}}>{todoData.date == "Pick Date" ? "Today" : todoData.date}</Text>
              <Text style={{fontWeight: 'bold', fontSize: 14}}>Time</Text>
              <Text style={{}}>{todoData.time}</Text>
            </View>
            <View style={{}}>
              <Text style={{fontWeight: 'bold', fontSize: 14}}>Due Date</Text>
              <Text style={{ marginBottom: 20}}>{diffDays} days</Text>
              <Text style={{fontWeight: 'bold', fontSize: 14}}>Due Time</Text>
              <Text style={{}}>{diffTime}</Text>
            </View>
          </View>
          <Text style={{fontSize: 20, color: '#6646ee', marginVertical: 50, opacity: 0.6}}>{todoData.desc}</Text>

        </View>
      </ImageBackground>
    </>
  )
}

export default DescriptionScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center'
  },
  card: {
    backgroundColor: '#efefef',
    borderRadius: 12,
    flex: 1,
    margin: 25,
    elevation: 5,
    paddingHorizontal: 13
  }
});