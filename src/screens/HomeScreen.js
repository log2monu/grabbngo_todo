
import React, { useContext, useEffect, useState } from 'react';
import { View, Text, StyleSheet, ImageBackground, TouchableOpacity, TextInput, Button } from 'react-native';
import { AuthContext } from '../navigation/AuthProvider';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon1 from 'react-native-vector-icons/Ionicons';
import DraggableFlatList from 'react-native-draggable-flatlist';
import Modal from 'react-native-modal';
import Task from '../components/Task';
import RadioButtonRN from 'radio-buttons-react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";

const HomeScreen = (props) => {
  const { user, logout } = useContext(AuthContext);

  const [titleValue, setTitleValue] = useState('')
  const [descValue, setDescValue] = useState('')
  const [urgent, setUrgent] = useState(true)
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [isTimePickerVisible, setTimePickerVisibility] = useState(false)
  const [date, setDate] = useState("Pick Date")
  const [time, setTime] = useState("Pick Time")
  // const [date, setDate] = useState(new Date().toLocaleDateString())
  // const [time, setTime] = useState(new Date().toTimeString().slice(0, 5))
  const [todos, setTodos] = useState([])
  const [modalVisible, setModalVisible] = useState(false)

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };
 
  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const showTimePicker = () => {
    setTimePickerVisibility(true);
  };
 
  const hideTimePicker = () => {
    setTimePickerVisibility(false);
  };
 
  // const handleConfirm = (date) => {
  //   console.warn("A date has been picked: ", date);
  //   hideDatePicker();
  // };

  const exampleData = [...todos].map((d, index) => ({
    key: `item-${index}`, // For example only -- don't use index as your key!
    label: index,
    title: titleValue,
    desc: descValue,
    backgroundColor: `rgb(${Math.floor(Math.random() * 255)}, ${index *
      5}, ${132})`
  }));

  const radioData = [
    {
      label: 'Urgent'
     },
     {
      label: 'Some day'
     }
    ];

  // handleNavigation = () => {
  //   props.navigation.navigate("Description")
  // }

  handleAddTodo = () => {
    if (titleValue.length > 0 && descValue.length > 0) {
      setTodos([...todos, { title: titleValue, desc: descValue, urgent, time, date, key: Date.now(), checked: false }])
      setTitleValue('');
      setDescValue('');
      setUrgent(true)
      setTime("Pick Time")
      setDate("Pick Date")
      setDatePickerVisibility(false)
      setTimePickerVisibility(false)
      setModalVisible(false)
    }
  }

  handleDeleteTodo = (id) => {
    setTodos(
      todos.filter((todo) => {
        if (todo.key !== id) return true
      })
    )
  }

  handleChecked = (id) => {
    setTodos(
      todos.map((todo) => {
        if (todo.key === id) todo.checked = !todo.checked;
        return todo;
      })
    )
  }

  toggleModal = () => {
    setModalVisible(!modalVisible)
    if (modalVisible == false) {
      setTitleValue('');
      setDescValue('');
      setUrgent(true)
      setTime("Pick Time")
      setDate("Pick Date")
      setDatePickerVisibility(false)
      setTimePickerVisibility(false)
    }
  }
  return (
    <ImageBackground source={require("../images/bg.jpg")} style={styles.container}>
      <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems :'center', marginHorizontal: 20, marginVertical: 25}}>
        <Text style={styles.text}>TODOS</Text>
        <TouchableOpacity onPress={()=> logout()} style={{padding:10, backgroundColor:'#6646ee', borderRadius: 100, elevation: 2,  }}>
          <Icon name={'logout'} color={'#fff'} size={17}/>
        </TouchableOpacity>
      </View>
      
      
      <DraggableFlatList
          data={todos}
          renderItem={(task)=>(
            <Task
              drag={task.drag}
              title={task.item.title}
              item= {task.item}
              key={task.item.key}
              checked={task.item.checked}
              setChecked={() => {console.warn("vkj :", task)
              handleChecked(task.item.key)}}
              handleNavigation={props.navigation}
              delete={() => handleDeleteTodo(task.item.key)}
            />
          )}
          keyExtractor={(item, index) => `draggable-item-${item.key}`}
          onDragEnd={({ data }) => setTodos(data)}
        />

        <TouchableOpacity onPress={()=> toggleModal()} style={{padding:10, backgroundColor:'#6646ee', borderRadius: 100, elevation: 2, position: 'absolute', bottom: 15, right: 15}}>
          <Icon1 name={'add'} color={'#fff'} size={34}/>
        </TouchableOpacity>

        <Modal  
          isVisible={modalVisible} 
          animationIn={'slideInUp'}
          animationInTiming={650}
          animationOut={'slideOutDown'}
          animationOutTiming={650}
          onBackButtonPress={()=> setModalVisible(false)} 
          onBackdropPress={()=> setModalVisible(false)}
          style={styles.modalContainer}
          >
            <View style={{  justifyContent: 'center', width :'85%'}}>
              <TextInput
                style={{...styles.textInput,marginBottom: 20}}
                multiline={false}
                onChangeText={(value) => setTitleValue(value)}
                placeholder={'Title'}
                placeholderTextColor="grey"
                value={titleValue}
              />
              <TextInput
                style={{...styles.textInput,marginBottom: 20}}
                multiline={true}
                onChangeText={(value) => setDescValue(value)}
                placeholder={'Description'}
                placeholderTextColor="grey"
                value={descValue}
              />

              <View style={{borderRadius: 6, borderWidth: 0.4, borderColor: 'grey', padding: 4, marginBottom: 20}}>
                <Text style={{fontSize: 15, fontSize: 20, paddingHorizontal: 10, color: 'grey'}}>Priority</Text>
                <RadioButtonRN
                  data={radioData}
                  initial={1}
                  activeColor={'#6646ee'}
                  boxDeactiveBgColor={'#f5f3df'}
                  style={{flexDirection: 'row', }}
                  boxStyle={{width: '48.5%', marginLeft: 3}}
                  selectedBtn={(e) => {
                    if (e.label == "Urgent")
                    {
                      setUrgent(true)
                    }else 
                    {
                      setUrgent(false)
                    }
                  }}
                />
              </View>
              {!urgent ?
                <>
                {/* <Button title={date} onPress={showDatePicker} /> */}
                <TouchableOpacity onPress={()=> showDatePicker()} style={{backgroundColor:'#6646ee', borderRadius: 8, elevation: 2, marginVertical: 15}}>
                  <Text style={{textAlign: 'center', color: 'white', paddingHorizontal: 25, paddingVertical: 8, fontSize: 17}}>{date}</Text>
                </TouchableOpacity>
                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="date"
                  onConfirm={(date)=>setDate(date.toLocaleDateString())}
                  onCancel={hideDatePicker}
                />
                {/* <Button title={time} onPress={showDatePicker} /> */}
                <TouchableOpacity onPress={()=> showTimePicker()} style={{backgroundColor:'#6646ee', borderRadius: 8, elevation: 2, marginBottom: 15}}>
                  <Text style={{textAlign: 'center', color: 'white', paddingHorizontal: 25, paddingVertical: 8, fontSize: 17}}>{time}</Text>
                </TouchableOpacity>
                <DateTimePickerModal
                  isVisible={isTimePickerVisible}
                  mode='time'
                  onConfirm={(time)=> setTime(time.toLocaleTimeString())}
                  onCancel={hideTimePicker}
                />
                </> : 
                <>
                  <TouchableOpacity onPress={()=> showTimePicker()} style={{backgroundColor:'#6646ee', borderRadius: 8, elevation: 2, marginBottom: 15}}>
                    <Text style={{textAlign: 'center', color: 'white', paddingHorizontal: 25, paddingVertical: 8, fontSize: 17}}>{time}</Text>
                  </TouchableOpacity>
                  <DateTimePickerModal
                    isVisible={isTimePickerVisible}
                    mode='time'
                    onConfirm={(time)=> setTime(time.toLocaleTimeString())}
                    onCancel={hideTimePicker}
                  />
                </>
              }
              <TouchableOpacity onPress={()=> handleAddTodo()} style={{backgroundColor:'#6646ee', borderRadius: 8, elevation: 2, justifyContent: 'center', width: 200, alignSelf: 'center'}}>
                <Text style={{textAlign: 'center', color: 'white', paddingVertical: 12, fontSize: 18, fontWeight: 'bold'}}>Add</Text>
              </TouchableOpacity>
            </View>

        </Modal>
      
    </ImageBackground>
  );
}

export default HomeScreen

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  text: {
    fontSize: 20,
    color: '#fff',
    fontWeight: 'bold'
  },
  textInput: {
    backgroundColor:'#f5f3df',
    borderRadius: 6,
    fontSize: 20,
    paddingHorizontal: 10,
    borderWidth: 1,
    borderColor: '#6646ee',
    color: '#6646ee'
  },
  modalContainer: {
    backgroundColor: '#f5f3df', 
    borderRadius: 8, 
    alignItems: 'center', 
    justifyContent: 'center',
  }
});